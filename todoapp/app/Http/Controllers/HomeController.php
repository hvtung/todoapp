<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function homepage()
    {
        return view('welcome');
    }
    public function todo()
    {
        return view('todo');
    }
    public function register()
    {
        return view('register');
    }
}
