<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/homepage', [App\Http\Controllers\HomeController::class, 'homepage'])->name('homepage');
Route::get('/todo', [App\Http\Controllers\HomeController::class, 'todo'])->name('todo');
Route::get('/dangky', [App\Http\Controllers\HomeController::class, 'register'])->name('register');
Auth::routes();